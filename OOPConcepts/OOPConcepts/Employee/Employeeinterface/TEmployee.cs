﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPConcepts.Employee.Employeeinterface
{
    class TEmployee : IPartEmployee, IFullEmployee
    {
        public TEmployee() //Default constructor
        {
            Console.WriteLine("TEmployee class constructor is class");
        }
        public TEmployee(string type) //Default constructor
        {
            Console.WriteLine("Employee type is " + type);
        }
        void IPartEmployee.GetEmployeeType()
        {

            Console.WriteLine("Employee type is parttime");
        }
        void IFullEmployee.GetEmployeeType()
        {
            Console.WriteLine("Employee type is Fulltime");
        }
    }
}
