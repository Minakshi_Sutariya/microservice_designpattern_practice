﻿using OOPConcepts.Designpatterns;
using OOPConcepts.Designpatterns.Factorypattern;
using OOPConcepts.Designpatterns.Prototype;
using OOPConcepts.Employee;
using OOPConcepts.Employee.Employeeinterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPConcepts
{
    class Program
    {
        static void Main(string[] args)
        {
            //Abstraction concepts
            Console.WriteLine("Full Employee Detail");

            FullTimeEmployee fullTimeEmployee = new FullTimeEmployee();
            fullTimeEmployee.FirstName = "Minakshi";
            fullTimeEmployee.LastName = "Sutariya";
            fullTimeEmployee.AnnualSalary = 10000;

            Console.WriteLine("FullName: " + fullTimeEmployee.GetFullName());
            Console.WriteLine("Monthly Salary: " + fullTimeEmployee.GetMonthlySalary());
            Console.WriteLine("--------------");

            Console.WriteLine("Contract Employee Detail");
            contractEmployee cte = new contractEmployee();
            cte.ID = 101;
            cte.FirstName = "Rahul";
            cte.LastName = "Jadav";
            cte.HourlyPay = 100;
            cte.TotalHours = 8;

            Console.WriteLine("FullName: " + cte.GetFullName());
            Console.WriteLine("Monthly Salary: " + cte.GetMonthlySalary());

            //Same method call from two diffrent interface
            IFullEmployee fullEmployee = new TEmployee();
            fullEmployee.GetEmployeeType();
            IPartEmployee partEmployee = new TEmployee();
            partEmployee.GetEmployeeType();


            //Constructor calling
            TEmployee tEmployee1 = new TEmployee(); //Default
            TEmployee tEmployee2 = new TEmployee("Contractor"); //Parameterized



            //Singltone  instance implementation
            Console.WriteLine("Addition: " + Singletone.Instance.sum(3, 4));
            Console.WriteLine("Substraction: " + Singletone.Instance.sum(5, 4));

           


            //Prtotype pattern
            ConcretePrototype concretePrototype = new ConcretePrototype("Hello");       // Create  instances
            ConcretePrototype p1 = (ConcretePrototype)concretePrototype.Clone();      //clone /copied here
            Console.WriteLine("Cloned: {0}", p1.Str_);


            //Strategy pattern
            contex contex1 = new contex(new Strategy1());  // Create  instances for subclasses
            contex1.Getinstance();
            contex contex2 = new contex(new Strategy2());
            contex2.Getinstance();


            //Factory pattern
            Console.WriteLine("Enter Your report type");
            string reporttype = Console.ReadLine();
            Report report = ReportFactory.getobject(reporttype);
            report.GetReport();


            Console.ReadLine();
        }
    }
}
