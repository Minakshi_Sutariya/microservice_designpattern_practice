﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPConcepts.Designpatterns
{
    abstract class Strategy
    {
        public abstract void Call();

    }
    class Strategy1 : Strategy
    {
        public override void Call()
        {
            Console.WriteLine("Call method of Strategy1 ");
        }
    }
    class Strategy2 : Strategy
    {
        public override void Call()
        {
            Console.WriteLine("Call method of Strategy2 ");
        }
    }
    class contex
    {
        public Strategy _Strategy;
        public contex(Strategy str)
        {
            this._Strategy = str;
        }
        public void Getinstance()
        {
            _Strategy.Call();
        }
    }

}
