﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPConcepts.Designpatterns.Factorypattern
{
    public class PDFReport : Report
    {
        public override void GetReport()
        {
            Console.WriteLine("Pdf Report generate");
        }

    }
}
