﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPConcepts.Designpatterns.Factorypattern
{
    public abstract class Report
    {
        public abstract void GetReport();
    }
}
