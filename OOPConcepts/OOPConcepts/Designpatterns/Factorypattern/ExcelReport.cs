﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPConcepts.Designpatterns.Factorypattern
{
    public class ExcelReport : Report
    {
        public override void GetReport()
        {
            Console.WriteLine("Excel Report generate");
        }

    }
}
