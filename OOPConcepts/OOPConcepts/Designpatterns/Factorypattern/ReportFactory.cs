﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPConcepts.Designpatterns.Factorypattern
{
    public class ReportFactory
    {
      static  Report report = null;
        public static Report getobject(string type)
        {
            switch (type.ToLower())
            {
                case "excel":
                    report = new ExcelReport();
                    break;
                case "pdf":
                    report = new PDFReport();
                    break;
                default:
                    break;
            }
            return report;
        }
    }
}
