﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPConcepts.Designpatterns.Prototype
{
    abstract class Prototype
    {
        /// <summary>
        /// Clone/Copy the created instance is  prototype
        /// </summary>
        private string Str;
        public Prototype(string s)
        {
            this.Str = s;
        }
        public string Str_
        {
            get
            {
                return Str;
            }
        }
        public abstract Prototype Clone();
    }
}
