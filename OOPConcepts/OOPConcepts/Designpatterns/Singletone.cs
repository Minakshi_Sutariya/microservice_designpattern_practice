﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPConcepts.Designpatterns
{
    public sealed class Singletone
    {
        //A static variable that holds a reference to the single created instance, if any.
        private static Singletone instance = null;
        private Singletone()
        {
            //A single constructor, that is private and parameterless.
        }
        public static Singletone Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Singletone();
                }
                return instance;
            }
        }
        public int sum(int a,int b)
        {
            return a + b;
        }
        public int substraction(int a, int b)
        {
            return a - b;
        }
    }
}
