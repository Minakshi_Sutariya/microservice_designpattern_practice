﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Customcontrol_Demo
{
    public partial class TestService : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                //You can use By adding reference.
                var Service = new MyWebservice();
                Label2.Text = Service.HelloWorld();


                //You can use By HttpWebrequest
                var url = "http://localhost:64297/MyWebservice.asmx/GetName";

                var webrequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
                webrequest.Method = "Post";
                string data = "Name=Rahul";//for passing parameters
                byte[] dataStream = Encoding.UTF8.GetBytes(data);
                webrequest.ContentType = "application/x-www-form-urlencoded";
                webrequest.ContentLength = dataStream.Length;
                Stream newStream = webrequest.GetRequestStream();
                // Send the data.
                newStream.Write(dataStream, 0, dataStream.Length);
                newStream.Close();
                using (var response = webrequest.GetResponse())
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var result = reader.ReadToEnd();
                    Label1.Text = Convert.ToString(result);
                }

            }

        }
    }
}