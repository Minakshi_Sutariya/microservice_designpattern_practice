﻿using System;
using System.Collections.Generic;
using System.Web.UI;

namespace WebForm_Demo
{
    public partial class Registration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //   
            }
        }
        /// <summary>
        /// Submit button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {

            Databaseconnection databaseconnection = new Databaseconnection();

            string Name = TextBox1.Text;
            int Age = int.Parse(TextBox5.Text.ToString());
            string password = TextBox2.Text;
            string Email = TextBox4.Text;
            DateTime DOB = DateTime.Parse(TextBox6.Text.ToString());
            string Usrname = TextBox7.Text;

            string message = databaseconnection.save("[dbo].[Sp_Registration]", Name, Age, DOB, password, Usrname, Email);
            Response.Write(@"<script language='javascript'>alert('" + message + " .');</script>");
            clear();
        }
        public void clear()
        {
            TextBox1.Text = string.Empty;
            TextBox5.Text = string.Empty;
            TextBox2.Text = string.Empty;
            TextBox3.Text = string.Empty;
            TextBox4.Text = string.Empty;
            TextBox7.Text = string.Empty;
            TextBox6.Text = string.Empty;
        }

    }
}