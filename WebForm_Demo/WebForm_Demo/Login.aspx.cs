﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm_Demo
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.User.Identity.IsAuthenticated)
            {
                Response.Redirect(FormsAuthentication.DefaultUrl);
            }

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

            string username = txtUsername.Text.Trim();
            string password = txtPassword.Text.Trim();
            int userId = 0;
            Databaseconnection databaseconnection = new Databaseconnection();
            userId = databaseconnection.login(username, password);
            switch (userId)
            {
                case -1:
                    dvMessage.Visible = true;
                    lblMessage.Text = "Username and/or password is incorrect.";
                    break;
                default:
                    if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                    {
                        FormsAuthentication.SetAuthCookie(username, chkRememberMe.Checked);
                        Response.Redirect(Request.QueryString["ReturnUrl"]);
                    }
                    else
                    {
                        FormsAuthentication.RedirectFromLoginPage(username, chkRememberMe.Checked);
                    }
                    break;
            }
        }

    }
}