﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebForm_Demo
{
    public class Databaseconnection
    {
        SqlConnection conn;
        readonly string connstring = @"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=Employeedb;Integrated Security=True";

        public string save(string sp_string, string name, int age, DateTime birthdate, string Password, string Username, string Email)
        {
            conn = new SqlConnection(connstring);
            conn.Open();
            SqlCommand comm = new SqlCommand(sp_string, conn);
            comm.CommandType = CommandType.StoredProcedure;

            comm.Parameters.AddWithValue("@Name", name);
            comm.Parameters.AddWithValue("@Age", age);
            comm.Parameters.AddWithValue("@Birth_Date", birthdate);
            comm.Parameters.AddWithValue("@Password", Password);
            comm.Parameters.AddWithValue("@Username", Username);
            comm.Parameters.AddWithValue("@Email", Email);

            try
            {
                comm.ExecuteNonQuery();
                return "Record Saved";
            }
            catch (Exception ex)
            {
                return "Record Not Saved";
            }
            finally
            {
                conn.Close();
            }
        }
        public int login(string username, string password)
        {
            using (SqlConnection con = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand("Validate_User"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Username", username);
                    cmd.Parameters.AddWithValue("@Password", password);
                    cmd.Connection = con;
                    con.Open();
                    int userId = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                    return userId;
                }
            }
        }
    }
}