﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="WebForm_Demo.Registration" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="form-group" style="padding: 12px 400px 24px;">
        <table class="auto-style1">
            <tr>
                <td>Name :</td>
                <td>
                    <asp:TextBox ID="TextBox1" CssClass="form-control " runat="server" required></asp:TextBox>
                    <br />
                </td>
            </tr>
            <tr>
                <td>Password</td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" TextMode="Password" required></asp:TextBox>
                    <br />
                </td>
            </tr>
            <tr>
                <td>Confirm Password</td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" TextMode="Password" required></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server"
                        ControlToCompare="TextBox2" ControlToValidate="TextBox3" ErrorMessage="password not match">
                    </asp:CompareValidator><br />
                </td>
            </tr>
            <tr>
                <td>Username</td>
                <td>
                    <asp:TextBox ID="TextBox7" runat="server" CssClass="form-control" required></asp:TextBox><br />
                </td>
            </tr>
            <tr>
                <td>Age</td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server" CssClass="form-control" TextMode="Number" required></asp:TextBox><br />
                </td>
            </tr>
            <tr>
                <td>DOB</td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server" CssClass="form-control" TextMode="Date" required></asp:TextBox><br />
                </td>
            </tr>
            <tr>
                <td>Email</td>
                <td>
                    <asp:TextBox TextMode="email" CssClass="form-control" ID="TextBox4" runat="server" required></asp:TextBox><br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Button1" Class="btn btn-primary" runat="server" Text="Save" OnClick="Button1_Click" />
                    <asp:Button ID="Button2" Class="btn btn-warning" runat="server" Text="Reset" OnClientClick="theForm.reset();return false;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
