﻿using myMicroservice.Models;
using System.Collections.Generic;

namespace ASp.netcore_EFCore.Repository
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetEmployee();
        int AddOrEdit(Employee Model);
        string Delete(int id);
    }
}
