﻿using Microsoft.EntityFrameworkCore;
using myMicroservice.Models;
using System.Collections.Generic;
using System.Linq;

namespace ASp.netcore_EFCore.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly EmployeedbContext db;
        public EmployeeRepository(EmployeedbContext employeedbContext)
        {
            this.db = employeedbContext;

        }

        public int AddOrEdit(Employee Model)
        {
            if (db != null)
            {
                if (Model.Id == 0)
                {
                    db.Employee.Add(Model);
                    db.SaveChanges();
                    return 1;
                }
                if (Model.Id > 0)
                {
                    db.Employee.Update(Model);
                    db.SaveChanges();
                    return 2;
                }
            }
            return 0;
        }

        public string Delete(int id)
        {
            string message = null;
            var employee=db.Employee.Find(id);
            if(employee!=null)
            {
                db.Employee.Remove(employee);
                db.SaveChanges();
                message = "Deleted Successfully";
            }
            return message; 
        }

        public IEnumerable<Employee> GetEmployee()
        {
            var employeelist = db.Employee.ToList();
               return employeelist;
        }
      
    }
}
