﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;

namespace myMicroservice
{
    public class CustomAuthorizeAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var headers = context.HttpContext.Request.Headers;
            bool isauthorized = false;
            if (headers.Keys.Contains("auth_token"))
            {
                var headr = headers.FirstOrDefault(a => a.Key == "auth_token").Value.FirstOrDefault();
                if (headr != null)
                    isauthorized = string.Equals(headr, "Microservice");

            }
            if (!isauthorized)
            {
                context.Result = new ContentResult()
                {
                    Content = "OOps,Authntication failed",
                    StatusCode = 401
                };

            }

        }

    }
}
